const dotenv = require("dotenv");
dotenv.config();

module.exports = {
    mysql_db_host: process.env.MYSQL_DB_HOST,
    mysql_db_user: process.env.MYSQL_DB_USER,
    mysql_db_password: process.env.MYSQL_DB_PASSWORD,
    mysql_database_name: process.env.MYSQL_DATABASE_NAME,
    port: process.env.PORT || 5000,
};
