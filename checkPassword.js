const pool = require("./sql_connection");

const checkPassword = (body) => {
    let email = body.email;
    return new Promise((resolve, reject) => {
        pool.getConnection((error, connection) => {
            if (error) {
                reject(error);
            } else {
                connection.query(
                    `SELECT password FROM userdata WHERE email = ?`,
                    email,
                    (err, data) => {
                        if (err) {
                            reject(err);
                            connection.release();
                        } else {
                            if (
                                JSON.parse(JSON.stringify(data))[0].password ==
                                body.password
                            ) {
                                resolve({ matched: true });
                            } else {
                                resolve({ matched: false });
                            }

                            connection.release();
                        }
                    }
                );
            }
        });
    });
};

module.exports = checkPassword;
