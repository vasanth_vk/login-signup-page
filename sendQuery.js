const pool = require("./sql_connection");

const sendQuery = (myQuery) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            connection.query(myQuery, (err, results) => {
                if (err) {
                    reject(`${err.message}`);
                    connection.release();
                } else {
                    resolve(results);
                    connection.release();
                }
            });
        });
    });
};

module.exports = sendQuery;
