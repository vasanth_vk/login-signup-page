const sendQuery = require("./sendQuery");

const createTable = () => {
    let query = `CREATE TABLE IF NOT EXISTS userdata(
        firstname VARCHAR(100),
        lastname VARCHAR(100),
        email VARCHAR(255) PRIMARY KEY,
        password TEXT
    )`;

    sendQuery(query)
        .then((data) => {
            console.log("created user data table");
        })
        .catch((err) => {
            console.log(err);
        });
};

module.exports = createTable;
