const olSigninbtn = document.getElementById("ol-si");
const olSignupbtn = document.getElementById("ol-su");
const olSignin = document.querySelector(".signin-overlay");
const olSignup = document.querySelector(".signup-overlay");
const signUpcontainer = document.getElementById("signup-container");
const signIncontainer = document.getElementById("signin-container");
const SigninSubmit = document.getElementById("form-signin-btn");
const SignupSubmit = document.getElementById("form-signup-btn");
const SigninForm = document.getElementById("signin-form");
const SignupForm = document.getElementById("signup-form");

let userdata = [
    { "yourmail@gmail.com": "12345678" },
    { "vk@gmail.com": "987654321" },
    { "seven@gmail.com": "qwertyuiop" },
];

olSigninbtn.addEventListener("click", () => {
    olSignin.classList.toggle("ol-si-active");
    olSignup.classList.toggle("ol-su-active");
    signIncontainer.classList.toggle("mobile-inactive");
    signUpcontainer.classList.toggle("mobile-inactive");
});

olSignupbtn.addEventListener("click", () => {
    olSignup.classList.toggle("ol-su-active");
    olSignin.classList.toggle("ol-si-active");
    signIncontainer.classList.toggle("mobile-inactive");
    signUpcontainer.classList.toggle("mobile-inactive");
});

SigninForm.addEventListener("submit", (event) => {});
