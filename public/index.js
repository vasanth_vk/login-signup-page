const olSigninbtn = document.getElementById("ol-si");
const olSignupbtn = document.getElementById("ol-su");
const olSignin = document.querySelector(".signin-overlay");
const olSignup = document.querySelector(".signup-overlay");
const signUpcontainer = document.getElementById("signup-container");
const signIncontainer = document.getElementById("signin-container");
const SigninSubmit = document.getElementById("form-signin-btn");
const SignupSubmit = document.getElementById("form-signup-btn");
const SigninForm = document.getElementById("signin-form");
const SignupForm = document.getElementById("signup-form");
const modalContainer = document.querySelector(".modal-container");
const modalClosebtn = document.querySelector(".modal-close");
const modalText = document.querySelector(".modal-text");

let userdata = [
    { "yourmail@gmail.com": "12345678" },
    { "vk@gmail.com": "987654321" },
    { "seven@gmail.com": "qwertyuiop" },
];

olSigninbtn.addEventListener("click", () => {
    olSignin.classList.toggle("ol-si-active");
    olSignup.classList.toggle("ol-su-active");
    signIncontainer.classList.toggle("mobile-inactive");
    signUpcontainer.classList.toggle("mobile-inactive");
});

olSignupbtn.addEventListener("click", () => {
    olSignup.classList.toggle("ol-su-active");
    olSignin.classList.toggle("ol-si-active");
    signIncontainer.classList.toggle("mobile-inactive");
    signUpcontainer.classList.toggle("mobile-inactive");
});

SigninForm.addEventListener("submit", (event) => {
    // modalText.textContent = "INVALID EMAIL/PASSWORD";
    setTimeout(() => {
        modalContainer.classList.remove("display-modal");
    }, 5000);
});

modalClosebtn.addEventListener("click", () => {
    modalContainer.classList.remove("display-modal");
});

SignupForm.addEventListener("submit", (event) => {
    modalText.textContent = "SIGNUP SUCCESSFUL";
    olSignup.classList.toggle("ol-su-active");
    olSignin.classList.toggle("ol-si-active");
    signIncontainer.classList.toggle("mobile-inactive");
    signUpcontainer.classList.toggle("mobile-inactive");
    modalContainer.classList.add("display-modal");
    setTimeout(() => {
        modalContainer.classList.remove("display-modal");
    }, 5000);
});

modalClosebtn.addEventListener("click", () => {
    modalContainer.classList.remove("display-modal");
});
