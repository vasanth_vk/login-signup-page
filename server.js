const express = require("express");
const path = require("path");
const app = express();
const { port } = require("./config");
const cors = require("cors");
const createTable = require("./createTable");
const checkUserExists = require("./checkUserExists");
const createNewUser = require("./createNewUser");
const checkPassword = require("./checkPassword");

app.use(cors());

app.use(express.json());
app.use(
    express.urlencoded({
        extended: true,
    })
);

app.use(express.static(path.join(__dirname, "./public")));

app.engine("html", require("ejs").renderFile);
app.set("views", path.join(__dirname, "./public"));
app.set("view engine", "html");

app.set("json spaces", 4);

app.get("/", (req, res) => {
    res.render("./index.html");
});

app.post("/signin", (req, res) => {
    console.log(req.body);
    checkUserExists(req.body)
        .then((data) => {
            if (data.userExists) {
                return checkPassword(req.body);
            } else {
                return { matched: false };
            }
        })
        .then((data) => {
            console.log(data);
            if (data.matched) {
                res.render("./login.html");
            } else {
                res.render("./invaliduser.html");
            }
        })
        .catch((err) => {
            console.log(err);
        });
});

app.post("/signup", (req, res) => {
    console.log(req.body);
    checkUserExists(req.body)
        .then((data) => {
            console.log(data);
            if (data.userExists) {
                res.render("userexists.html");
            } else {
                console.log(data);
                createNewUser(req.body);
                res.sendStatus(204);
            }
        })
        .catch((err) => {
            console.log(err);
        });
});

// createTable();
app.listen(port, () => {
    console.log(`Sever listening at ${port}`);
});
