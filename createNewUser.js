const pool = require("./sql_connection");

const createNewUser = (body) => {
    let values = Object.values(body);
    pool.getConnection((err, connection) => {
        connection.query(
            `INSERT INTO userdata VALUES ?`,
            [[values]],
            (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("created new user");
                }
            }
        );
    });
};

module.exports = createNewUser;
