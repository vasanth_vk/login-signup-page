const pool = require("./sql_connection");

const checkUserExists = (body) => {
    console.log(body);
    let email = body.email;
    return new Promise((resolve, reject) => {
        pool.getConnection((error, connection) => {
            if (error) {
                reject(error);
            } else {
                connection.query(
                    `SELECT * FROM userdata WHERE email = ?`,
                    email,
                    (err, data) => {
                        if (err) {
                            reject(err);
                            connection.release();
                        } else {
                            data = JSON.parse(JSON.stringify(data));
                            console.log(data);
                            if (Array.isArray(data) && data.length) {
                                console.log("user exists");
                                resolve({ userExists: true });
                                connection.release();
                            } else {
                                console.log("user doesn't exists");
                                resolve({ userExists: false });
                                connection.release();
                            }
                        }
                    }
                );
            }
        });
    });
};

module.exports = checkUserExists;
